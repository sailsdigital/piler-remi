<?php

$_['text_action'] = "Ação";
$_['text_active_incoming_queue'] = "fila activo + incoming";
$_['text_active_incoming_queue_sender'] = "fila activo + incoming queue x remetentes";
$_['text_ad_sync_status'] = "Status de sincronização AD";
$_['text_add'] = "Adicionar";
$_['text_add_new_email_address'] = "Novo email";
$_['text_add_new_domain'] = "Novo domínio";
$_['text_add_new_group'] = "Adicionar grupo";
$_['text_add_new_rule'] = "Adicionar regra";
$_['text_add_new_user_alias'] = "Adicionar novo usuário";
$_['text_add_policy'] = "Adicionar nova política";
$_['text_administration'] = "Administração";
$_['text_admin_user'] = "Usuário admin";
$_['text_advanced'] = "Avançado";
$_['text_advanced_search'] = "Pesquisa avançada";
$_['text_all'] = "todos";
$_['text_any'] = "qualquer";
$_['text_applied'] = "Applied";
$_['text_apply_changes'] = "Apply changes";
$_['text_archive_size'] = "Tamanho do arquivo";
$_['text_archived_messages'] = "Mensagens arquivadas";
$_['text_archiving_rules'] = "Regras de não arquivamento";
$_['text_attachment'] = "Anexo";
$_['text_attachment_size'] = "Tamanho do anexo";
$_['text_attachment_type'] = "Tipo de anexo";
$_['text_audit'] = "Auditoria";

$_['text_back'] = "Voltar";
$_['text_body'] = "Mensagem";
$_['text_bulk_edit_selected_uids'] = "Seleção de UIDs";
$_['text_bulk_restore_selected_emails'] = "Reenviar  emails selecionados";
$_['text_bulk_update_selected_uids'] = "Atualização de UIDs selecionados ";

$_['text_cancel'] = "Cancelar";
$_['text_change_user_settings'] = "alterar configuração de usuário";
$_['text_clienthost'] = "Client host";
$_['text_close'] = "Fechar";
$_['text_compressed'] = "comprimido";
$_['text_confirm_to_reset_counters'] = "Confirme para resetar os contadores";
$_['text_content_filter'] = "Filtro de conteúdo";
$_['text_conversation_available'] = "Conversão disponível";
$_['text_copied'] = "Copiado";
$_['text_counters'] = "Contadores:";
$_['text_cpu_load'] = "CPU, carga de";
$_['text_cpu_usage'] = "CPU, uso de";

$_['text_daily_quarantine_report'] = "Relatório diário de quarentena";
$_['text_daily_quarantine_report_status'] = "Status do relatório diário de quarentena";
$_['text_daily_report'] = "Relatório diário";
$_['text_daily_piler_report'] = "Relatório diário do piler";
$_['text_database_emails'] = "emails cadastrados na base de dados do piler";
$_['text_date'] = "Data";
$_['text_date_from'] = "Data de";
$_['text_date_to'] = "Data até";
$_['text_days'] = "Dias";
$_['text_days2'] = "dias";
$_['text_days_to_retain'] = "Dias a reter";
$_['text_deferred_queue'] = "fila de espera";
$_['text_deferred_queue_sender'] = "fila de espera x enviados";
$_['text_delay'] = "Atraso";
$_['text_deleted_users'] = "excluído";
$_['text_deliver'] = "Entregar";
$_['text_delivered'] = "Entregue";
$_['text_deliver_and_train_selected_messages'] = "Entregar e treinar mensagens selecionadas";
$_['text_deliver_and_train_selected_messages_as_ham'] = "Entregar e treinar mensagens selecionadas como útil";
$_['text_deliver_selected_messages'] = "Entregar mensagens selecionadas";
$_['text_description'] = "Descrição";
$_['text_direction'] = "Direção";
$_['text_disk_usage'] = "Disco, uso de:";
$_['text_disable'] = "Desabiltar";
$_['text_disabled'] = "desabilitado";
$_['text_dn_asterisk_means_skip_sync'] = "Asterisco (*) significa que este usuário não participará da sincronização AD";
$_['text_domain'] = "Domínios";
$_['text_domains'] = "Domínio(s)";
$_['text_domainname'] = "Nome do domínio";
$_['text_download_attachment2'] = "download anexo";
$_['text_download_message'] = "Download mensagem (EML)";
$_['text_download_message2'] = "download mensagem";

$_['text_edit'] = "Editar";
$_['text_edit_group'] = "Editar grupo";
$_['text_edit_user'] = "Editar usuário";
$_['text_edit_or_view'] = "Editar / Visualizar";
$_['text_email'] = "Email";
$_['text_email_addresses'] = "Emails";
$_['text_email_aliases'] = "Emails aliases";
$_['text_email_in_unknown_domain'] = "Email pertence a um domínio desconhecido";
$_['text_empty_search_criteria'] = "'Critério vazio'";
$_['text_empty_search_result'] = "Pesquisa sem resultado";
$_['text_enable'] = "Permitir";
$_['text_enabled'] = "habilitado";
$_['text_enter_one_email_address_per_line'] = "Digite um email por linha";
$_['text_enter_one_group_per_line'] = "Digite um grupo por linha";
$_['text_enter_search_terms'] = "Digite sua pesquisa ou clique na flecha";
$_['text_error'] = "Erro";
$_['text_exact_domain_name_or_email_address'] = "nome do domínio ou do email exato";
$_['text_exclude'] = "Excluir";
$_['text_existing_domains'] = "Domínios existentes";
$_['text_existing_email'] = "Email existente";
$_['text_existing_folders'] = "Pastas existentes";
$_['text_existing_groups'] = "Grupos existentes";
$_['text_existing_policies'] = "Políticas existentes";
$_['text_existing_rules'] = "Regras existentes";
$_['text_existing_user'] = "Usuário existente";
$_['text_existing_users'] = "Usuários existentes";
$_['text_expert'] = "Expert";
$_['text_expert_search'] = "Pesquisa expert";

$_['text_failed'] = "falhou";
$_['text_failed_to_add'] = "Falhou ao adicionar";
$_['text_failed_to_change_password'] = "Falha ao alterar senha";
$_['text_failed_to_deliver'] = "Falha ao entregar";
$_['text_failed_to_mark_for_removal'] = "Falha ao marcar para remoção";
$_['text_failed_to_modify'] = "Falha ao modificar";
$_['text_failed_to_remove'] = "Falha ao remover";
$_['text_failed_to_restore'] = "Falha ao reenviar";
$_['text_failed_to_update'] = "Falha ao atualizar";
$_['text_first'] = "Primeiro";
$_['text_folder'] = "Pasta";
$_['text_folders'] = "Pastas";
$_['text_from'] = "De";
$_['text_from_domain'] = "Domínio de";

$_['text_group_id'] = "Id do grupo";
$_['text_groupname'] = "Nome do grupo";
$_['text_groups'] = "Grupos";
$_['text_group_management'] = "Gestão de grupos";
$_['text_group_membership'] = "Membros do grupo";

$_['text_health'] = "Desempenho";
$_['text_health_monitor'] = "Monitor de desempenho";
$_['text_help'] = "Ajuda";
$_['text_history'] = "Histórico";
$_['text_home'] = "Home";

$_['text_image'] = "imagem";
$_['text_import'] = "Importar";
$_['text_import_users'] = "Importar usuários";
$_['text_import_users_from_LDAP'] = "Importar usuários de LDAP";
$_['text_inbound'] = "interno";
$_['text_install_sudo_apply'] = "Add the following to /etc/sudoers: 'www-data ALL=NOPASSWD: /etc/init.d/rc.piler reload'";
$_['text_internal'] = "interno";
$_['text_invalid_data'] = "Dados inválidos";
$_['text_invalid_email'] = "Email inválido";
$_['text_invalid_email_or_password'] = "Email ou senha inválido";
$_['text_invalid_gid'] = "GID inválido";
$_['text_invalid_password'] = "Senha inválida";
$_['text_invalid_policy_group'] = "Política de grupo inválida";
$_['text_invalid_policy_name'] = "Nome da política invalido";
$_['text_invalid_policy_setting'] = "Configuração de política inválida";
$_['text_invalid_uid'] = "UID invalido";
$_['text_invalid_username'] = "Nome de usuário inválido";
$_['text_ipaddr'] = "Endereço IP";
$_['text_language'] = "Idioma";
$_['text_last'] = "Último";
$_['text_last_update'] = "Última atualização";
$_['text_latest_emails'] = "Emails mais recentes";
$_['text_ldap_basedn'] = "LDAP base DN";
$_['text_ldap_binddn'] = "LDAP bind DN";
$_['text_ldap_bindpw'] = "LDAP bind password";
$_['text_ldap_host'] = "LDAP host";
$_['text_ldap_type'] = "LDAP type";
$_['text_load'] = "Carregar";
$_['text_loading'] = "carregando";
$_['text_logged_out'] = "Você não está logado";
$_['text_login'] = "Login";
$_['text_login2'] = "login";
$_['text_login_failed'] = "falha de login";
$_['text_login_via_google'] = "Login via conta do Google";
$_['text_logout'] = "Logout";
$_['text_logout2'] = "logout";

$_['text_maillog_status'] = "status do coletor do log de email";
$_['text_main_title'] = "clapf web UI";
$_['text_mapped_domain'] = "Domínio mapeado";
$_['text_marked_for_removal'] = "Mensagem marcada para remoção";
$_['text_memory_usage'] = "Memoria, uso de";
$_['text_message'] = "mensagem";
$_['text_messages'] = "mensagens";
$_['text_message_text'] = "Texto da mensagem";
$_['text_min_2_chars'] = "Min. 2 characeres";
$_['text_missing_data'] = "Faltando dados";
$_['text_missing_password'] = "Senha faltando";
$_['text_modify'] = "Alterar";
$_['text_monitor'] = "Monitor";
$_['text_months'] = "months";
$_['text_monthly_report'] = "Relatório mensal";

$_['text_new_users'] = "novo";
$_['text_next'] = "Próximo";
$_['text_no_domain_found'] = 'No domain found';
$_['text_no_email_found'] = 'No email found';
$_['text_no_message_in_the_quarantine'] = "Nenhuma mensagem na quarentena combinando com os critérios da pesquisa";
$_['text_no_records'] = "Sem registros";
$_['text_no_sender'] = "sem remetente";
$_['text_no_spam_message_in_the_quarantine_yet'] = "Ainda sem nenhuma mensagem de spam na quarentena";
$_['text_no_subject'] = "sem assunto";
$_['text_no_such_policy'] = "Política inexistente";
$_['text_non_existent_queue_directory'] = "Diretório de fila especificado não existe";
$_['text_non_existing_user'] = "Usuário não existente";
$_['text_notes'] = "Notas";
$_['text_not_found'] = "Não encontrado";
$_['text_not_running'] = "não executando";
$_['text_not_spam'] = "não é spam";
$_['title_not_found'] = "Página não encontrada";
$_['text_number_of_messages_in_quarantine'] = "Número de mensagens na quarentena combinando com os critérios da pesquisa";
$_['text_number_of_spam_messages_in_quarantine'] = "Número de mensagens spam na quarentena combinando com os critérios da pesquisa";

$_['text_off'] = "desligado";
$_['text_on'] = "ligado";
$_['text_other'] = "outros";
$_['text_outbound'] = "saida";

$_['text_password'] = "Senha";
$_['text_password_again'] = "Repetir senha";
$_['text_password_changed'] = "Senha alterada";
$_['text_password_mismatch'] = "Senhas divergentes";
$_['text_page_length'] = "Ítens por página";
$_['text_periodic_purge'] = "Expurgo periódico:";
$_['text_policy'] = "Política";
$_['text_policy_group'] = "Grupo de política";
$_['text_policy_name'] = "Nome da política";
$_['text_previous'] = "Anterior";
$_['text_processed_emails'] = "Emails processados";
$_['text_purge_all_messages_from_quarantine'] = "Expurgar todos as suas próprias mensagens da quarentena";
$_['text_purge_selected_messages'] = "Expurgar mensagens selecionadas";
$_['text_purged'] = "Expurgado";

$_['text_queue_status'] = "Status da fila";
$_['text_quick_search'] = "Busca rápida";

$_['text_realname'] = "Conta";
$_['text_recipient'] = "Recipiente";
$_['text_ref'] = "Referência";
$_['text_refresh_period'] = "Período de renovação";
$_['text_relay_details'] = "Detalhes relay";
$_['text_relay_status'] = "Status relay";
$_['text_remove'] = "Remover";
$_['text_remove_domain'] = "Remover domínio";
$_['text_remove_message'] = "Remover mensagem";
$_['text_remove_message2'] = "remover mensagem";
$_['text_remove_selected_uids'] = "Remover UIDs selecionados";
$_['text_remove_policy'] = "Remover política";
$_['text_remove_rule'] = "Remover regra";
$_['text_remove_this_policy'] = "Remover esta política";
$_['text_remove_this_group'] = "Remover este grupo";
$_['text_remove_this_user'] = "Remover este usuário";
$_['text_reset_counters'] = "Zerar contadores";

$_['text_restore'] = "reenviar";
$_['text_restore_message'] = "reenviar mensagem";
$_['text_restore_to_mailbox'] = "Reenviar mensagem";
$_['text_restored'] = "Reenviado.";
$_['text_result'] = "Resultado";
$_['text_retention_rules'] = "Regras de retenção";
$_['text_role'] = "Função";
$_['text_running'] = "processando";

$_['text_save'] = "Salvar";
$_['text_saved'] = "Salvando";
$_['text_save_search'] = "salvar pesquisa";
$_['text_save_search_terms'] = "Salvar string de pesquisa";
$_['text_saved_search_terms'] = "String de pesquisa salvo";
$_['text_search'] = "Pesquisar";
$_['text_search2'] = "pesquisar";
$_['text_search_emails'] = "Pesquisar emails";
$_['text_search_email_to_add'] = "Pesquisar emails para adicionar";
$_['text_search_folders'] = "Pesquisar pastas";
$_['text_search_folder_to_add'] = "Pesquisar pastas para adicionar";
$_['text_search_groups'] = "Pesquisar grupos";
$_['text_search_group_to_add'] = "Pesquisar grupos para adicionar";
$_['text_search_terms'] = "Pesquisar strings";
$_['text_select_action'] = "Selecionar ação";
$_['text_select_all'] = "Selcionar todos";
$_['text_select_recipients'] = "Selcionar recipiente";
$_['text_sender'] = "Remetente";
$_['text_sending_domains'] = "envio de domínios";
$_['text_server_name'] = "Nome do servidor";
$_['text_server_operating_system'] = "Sistema operacional";
$_['text_set'] = "Salvar";
$_['text_settings'] = "Configurações";
$_['text_simple'] = "Simples";
$_['text_simple_search'] = "Pesquisa simples";
$_['text_size'] = "Tamanho";
$_['text_smtp_status'] = "SMTP status";
$_['text_spam'] = "Spam";
$_['text_spam2'] = "spam";
$_['text_statistics'] = "Estatísticas";
$_['text_status'] = "Status";
$_['text_subject'] = "Assunto";
$_['text_submit'] = "Enviar";
$_['text_successful'] = "Sucesso";
$_['text_successfully_added'] = "Adicionado com sucesso";
$_['text_successfully_delivered'] = "Enviado com sucesso";
$_['text_successfully_modified'] = "Alterado com sucesso";
$_['text_successfully_removed'] = "Removido com sucesso";
$_['text_successfully_trained'] = "Treinado com sucesso";
$_['text_successfully_updated'] = "Atualizado com sucesso";
$_['text_swap_usage'] = "Swap, uso de:";

$_['text_tag_selected_messages'] = "Marcar(Tag) resultados de pesquisa";
$_['text_tagged'] = "Marcados";
$_['text_tags'] = "Tags";
$_['text_text'] = "Texto";
$_['text_text2'] = "texto";
$_['text_theme'] = "Tema";
$_['text_time'] = "Hora";
$_['text_to'] = "Para";
$_['text_to_domain'] = "Domínio para";
$_['text_too_short_password'] = "Senha muito curta";
$_['text_total'] = "total";
$_['text_total_ratio'] = "proporção total";
$_['text_total_query_time'] = "Tempo total de consulta SQL";
$_['text_total_users'] = "total";

$_['text_uids'] = "UIDs";
$_['text_unauthorized_domain'] = "Domínio não autorizado";
$_['text_unauthorized_download_attachment'] = "unauthorized attachment download";
$_['text_unauthorized_remove_message'] = "remoção de mensagem não autorizada";
$_['text_unauthorized_view_message'] = "visualização de mensagem não autorizada";
$_['text_unknown'] = "desconhecido";
$_['text_update_selected_uids'] = "Atualizar UIDs selecionados";
$_['text_uptime'] = "Uptime";
$_['text_user'] = "Usuário";
$_['text_users'] = "Usuários";
$_['text_user_id'] = "ID usuário";
$_['text_user_auditor'] = "Auditor";
$_['text_user_domainadmin'] = "Administrador de domínio";
$_['text_user_management'] = "Gestão de usuários";
$_['text_user_masteradmin'] = "Administrador master";
$_['text_user_read_only_admin'] = "Administrador somente de leitura";
$_['text_user_regular'] = "Usuário regular";
$_['text_userlist'] = "Lista de usários";
$_['text_username'] = "Nome de usuário";
$_['text_users_quarantine'] = "Quarentena de usuário";

$_['text_view_formatted_email'] = "Visualizar email formatado";
$_['text_view_header'] = "Visualizar cabeçalho";
$_['text_view_headers'] = "Visualizar cabeçalho";
$_['text_view_message'] = "Visualizar mensagem";
$_['text_view_message2'] = "visualizar mensagem";
$_['text_view_raw_email'] = "Visualizar email em formar RAW";
$_['text_view_user_quarantine'] = "Visualizar quarentena do usuário";

$_['text_warning_about_default_policy'] = "A política padrão pode ser configurado em clapf.conf";
$_['text_whitelist'] = "Lista branca";
$_['text_whitelist_settings'] = "Configuração de lista branca";
$_['text_with_attachment'] = "com anexo(s)";
$_['text_without_attachment'] = "sem anexo";

$_['text_years'] = "years";
$_['text_you_are'] = "Você é";
$_['text_you_are_not_admin'] = "Você não é um usuário admin";


$_['rcvd'] = "mensagens recebidas";
$_['virus'] = "mensagens infectadas";
$_['duplicate'] = "mensagens duplicadas";
$_['ignore'] = "mensagens ignoradas";
$_['counters_last_update'] = "contadores atualizados";

$_['text_24_hours'] = "24 horas";
$_['text_1_week'] = "1 semana";
$_['text_30_days'] = "30 dias";

$_['text_access_settings'] = 'Access Settings';
$_['text_access_setting_explanation'] = "You always have access to your own email addresses.  For auditor access to specific groups or domains, please contact your archive administrator.";
$_['text_display_settings'] = 'Display Settings';
$_['text_change_password'] = "Change Password";
$_['text_none_found'] = "None found";
$_['text_primary_domain'] = "Primary Domain";
$_['text_search_domains'] = "Search domains";
$_['text_search_domain_to_add'] = "Search domain to add";

$_['text_space_projection'] = 'Space Projection';
$_['text_average_messages_day'] = 'Average Messages per Day';
$_['text_average_message_size'] = 'Average Message + Metadata + Index Size';
$_['text_average_size_day'] = 'Average Size per Day';
$_['text_partition_full'] = 'Partition Projected to be Full in';
$_['text_usage_trend'] = 'Usage Trend';
$_['text_usage_increasing'] = 'Increasing';
$_['text_usage_decreasing'] = 'Decreasing';
$_['text_usage_neutral'] = 'Neutral';
$_['text_accounting'] = 'Archive Accounting';
$_['text_accounting_email'] = 'By-Email Accounting';
$_['text_accounting_domain'] = 'By-Domain Accounting';

$_['text_options'] = 'Options';
$_['text_spam_flag'] = 'Message Flagged as SPAM';
$_['text_attachment_flag'] = 'Message has Attachment';
$_['text_notes_flag'] = 'Message has Notes';
$_['text_tag_flag'] = 'Message is Tagged';
$_['text_verified_flag'] = 'Message is Verified';
$_['text_unverified_flag'] = 'Message Failed Verification';
$_['text_bulk_download'] = 'Bulk Download Selected Emails';
$_['text_clear'] = 'Clear';
$_['text_select_letter'] = 'Select Addresses by Letter';
$_['text_working'] = 'Working...';

$_['text_use_browser_settings'] = 'Use browser settings';

$_['text_sent'] = 'Sent';
$_['text_received'] = 'Received';
$_['text_oldest_record'] = 'Oldest record';
$_['text_newest_record'] = 'Newest Record';
$_['text_items'] = 'Items';
$_['text_average_size'] = 'Avg size';


?>
